import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "commune")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Access(AccessType.FIELD)
public class Maire {

	@XmlAttribute(name = "id")
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@XmlAttribute(name = "nom")
	@Column(name = "nom", length = 40)
	private String nom;
	
	@XmlAttribute(name = "age")
	@Column(name = "age")
	private int age;

	public Maire() {
	}

	public Maire(String nom, int age) {
		this.nom = nom;
		this.age = age;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Maire [id=" + id + ", nom=" + nom + ", age=" + age + "]";
	}
}
