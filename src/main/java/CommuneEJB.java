import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class CommuneEJB {

	@PersistenceContext(unitName = "tp-javaee")
	private EntityManager em;

	
	public long create(Commune commune) {
		em.persist(commune);
		return commune.getId();
	}
}
