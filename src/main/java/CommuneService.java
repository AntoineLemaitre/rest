import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
@Transactional
public class CommuneService {
	@PersistenceContext(unitName = "tp-javaee")
	private EntityManager em;

	
	public long create(Commune commune) {
		em.persist(commune);
		return commune.getId();
	}

	public Commune findByCodePostal(String codePostal) {		
		String jpql = "select commune from Commune commune where commune.codePostal = :codePostal";
		Query query = em.createQuery(jpql);
		query.setParameter("codePostal", codePostal);
		
		return (Commune) query.getSingleResult();
	}

	public Commune findById(long id) {
		return em.find(Commune.class, id);
	}
	
	public Maire findMaireById(long id) {
		return em.find(Maire.class, id);
	}
	
	public long update(Commune commune) {
		em.merge(commune);
		return commune.getId();
	}

	public boolean delete(long id) {
		Commune commune = em.find(Commune.class, id);
		if (commune != null) {
			em.remove(commune);
			return true;
		} else {
			return false;
		}
	}
}
