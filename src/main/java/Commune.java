
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("serial")
@XmlRootElement(name = "commune")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Access(AccessType.FIELD)
public class Commune {

	@XmlAttribute(name = "id")
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	

	@XmlElement(name = "codeINSEE")
	@Column(length = 40)
	private String codeINSEE;
	
	@XmlElement(name = "nomCommune")
	@Column(length = 40)
	private String nomCommune;
	
	@XmlElement(name = "codePostal")
	@Column(length = 40)
	private String codePostal;
	
	@XmlElement(name = "libelleAchivement")
	@Column(length = 40)
	private String libelleAchivement;

	public Commune(String codeINSEE, String nomCommune, String codePostal, String libelleAchivement) {
		this.codeINSEE = codeINSEE;
		this.nomCommune = nomCommune;
		this.codePostal = codePostal;
		this.libelleAchivement = libelleAchivement;
	}

	public Commune() {
	}

	public String getCodeINSEE() {
		return codeINSEE;
	}

	public void setCodeINSEE(String codeINSEE) {
		this.codeINSEE = codeINSEE;
	}

	public String getNomCommune() {
		return nomCommune;
	}

	public void setNomCommune(String nomCommune) {
		this.nomCommune = nomCommune;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLibelleAchivement() {
		return libelleAchivement;
	}

	public void setLibelleAchivement(String libelleAchivement) {
		this.libelleAchivement = libelleAchivement;
	}

	@Override
	public String toString() {
		return "Commune [codeINSEE=" + codeINSEE + ", nomCommune=" + nomCommune + ", codePostal=" + codePostal
				+ ", libelleAchivement=" + libelleAchivement + "]";
	}
}
