
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/commune")
public class CommuneRS {

	@Inject
	private CommuneService communeService;
	
	@GET @Path("hello")
	public String ping() {
		return "Hello World!";
	}
	
	@GET @Path("id/{a}")
	public Response id(@PathParam("a") long i) {
		Response.status(200).status((int) i).build();
		return i % 2 == 0 ? Response.ok(i).build()  : 
							Response.status(404).build();
	}
	
	@GET
	@Path("paris/12")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Commune findById() {
		Commune paris = new Commune();
		return paris;
	}

	@GET
	@Path("75006")
	@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
	public Response findByCodePostal() {

		Commune commune = communeService.findByCodePostal("75006");
		if (commune != null) {
			return Response.ok(commune).build();
		} else {
			return Response.status(Status.NOT_FOUND.getStatusCode(), "Commune 75000 non trouvée")
					.build();
		}
	}
	
	@GET
	@Path("maire/6")
	@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
	public Response findMaireById() {

		Maire maire = communeService.findMaireById(6);
		if (maire != null) {
			return Response.ok(maire).build();
		} else {
			return Response.status(Status.NOT_FOUND.getStatusCode(), "Maire 6 non trouvée")
					.build();
		}
	}
	
	@GET
	@Path("{id}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
	public Response findById(@PathParam("id") long id) {

		Commune commune = communeService.findById(id);
		if (commune != null) {
			return Response.ok(commune).build();
		} else {
			return Response.status(Status.NOT_FOUND.getStatusCode(), "Commune " + id + " non trouvée")
					.build();
		}
	}
}
