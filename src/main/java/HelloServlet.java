
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class HelloServlet
 */
@WebServlet("/hello")
public class HelloServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public HelloServlet() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {

		HttpSession session = request.getSession(); 
		if (session.getAttribute("count") == null) {
			long count = 1L;
			session.setAttribute("count", count);
		} else {
			long count = (long)session.getAttribute("count");
			count++;
			session.setAttribute("count", count);
		}
		
		PrintWriter writer = response.getWriter();
		
		String id = request.getParameter("id");
		
		writer.printf("<p>Il est déjà %s !</p>", new Date());
		writer.print("<p>Served at: "+ request.getContextPath() + "</p>");
		writer.print("<p>Count = " + session.getAttribute("count") + "</p>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		
		doGet(request, response);
	}
}
